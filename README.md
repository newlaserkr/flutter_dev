## Resource related to Flutter Development.
- Flutter development research to build Cross flatform application.


## Setup development environment:
To setup to develop a flutter project, you'll have to follow the steps below:

Read more at https://flutter.dev/docs.

- [DoIt Flutter Git](https://github.com/rollcake86/doitflutter)
- [flutter](https://flutter.dev/docs)
- [dart](https://dart.dev/guides)
- [install Flutter sdk](https://flutter.dev/docs/get-started/install)
- [awesome-flutter](https://github.com/Solido/awesome-flutter)
- [coggle.it/diagram-Flutter](https://coggle.it/diagram/YEOnPKW-ynRebYqi/t/flutter)
- [GitBook](https://www.gitbook.com)
- [XMind](https://www.xmind.net/download/xmind8)